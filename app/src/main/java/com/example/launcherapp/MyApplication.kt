package com.example.launcherapp

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ResolveInfo
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import timber.log.Timber


open class MyApplication : Application() {
    lateinit var activity: Activity
    lateinit var reciever: MyReciever
    private lateinit var list: MutableLiveData<ArrayList<AppData>>

    // private lateinit var list_:LiveData<ArrayList<AppData>>
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        reciever = MyReciever()
        var intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED)
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        intentFilter.addDataScheme("package")
        applicationContext.registerReceiver(reciever, intentFilter)

        list = MutableLiveData<ArrayList<AppData>>()
        refreshList()


    }


    internal fun refreshList() {
        list.value = getListOfApp()
        Timber.d("refreshList")
    }

    private fun getListOfApp(): ArrayList<AppData> {
        var applist = ArrayList<AppData>()
        var list: List<ResolveInfo> = Intent(Intent.ACTION_MAIN).let {
            it.addCategory(Intent.CATEGORY_LAUNCHER)
            packageManager.queryIntentActivities(it, 0)
        }


        list.forEach {
            var app = AppData(
                it.loadLabel(packageManager) as String,
                it.activityInfo.packageName,
                it.loadIcon(packageManager) as Drawable
            )
            applist.add(app)
            Timber.d("app data: ${app.toString()}")
        }

        applist.sortBy {
            it.name
        }
        return applist
    }

    override fun onTerminate() {
        super.onTerminate()
        unregisterReceiver(reciever)
    }

    fun getAppList(): LiveData<ArrayList<AppData>> = list


    fun launchApp(pakageName: String) {
        try {
            var i = packageManager.getLaunchIntentForPackage(pakageName);
            startActivity(i);
        } catch (e: Exception) {
            Timber.e(e.toString())
        }

    }

    fun unIstallApp(pakageName: String) {
        try {
            Timber.d("uninstall pakage name: $pakageName")
            val intent = Intent(Intent.ACTION_DELETE)
            intent.data = Uri.parse("package:$pakageName")
            startActivity(intent)
        } catch (e: Exception) {
            Timber.e(e.toString())
        }
    }


}