package com.example.launcherapp

import android.graphics.drawable.Drawable

data class AppData(
    val name: String,
    val pakageName: String,
    val icon: Drawable
)
